# README #

This is a quick configuration setup for Bashrc file, Vim and Git files 
### What is this repository for? ###

* By cloning this repo you get every thing functionning as expected: vim with advanced plugins, Git configurations and bashrc support for git branches, repo status...
* version 0.1

### How do I get set up? ###

* Clone the repo along with all it's submodule on your home directory: 

```
#!sh

$ git clone --recursive https://haouariano@bitbucket.org/haouariano/.dotfiles.git 
```
* Make sure you have vim with ruby support and ruby-dev installed:
```
#!sh

$ sudo apt install vim-nox ruby-dev
```
* run these commands to create .vim dir, .vimrc, .bashrc and .gitconfig links and compile Command-T plug-in for vim:

```
#!sh

$ cd .dotfiles
$ chmod +x install.sh
$ ./install.sh
```

### Who do I talk to? ###

* Repo owner haouariano@gmail.com
