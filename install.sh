#!/bin/bash
# Symbolic links script generator
echo Creating links ...
rm  ~/.vim
rm  ~/.bashrc
rm  ~/.vimrc
rm  ~/.gitconfig
rm  ~/.nvm

ln -s $PWD/.vim/ ~/.vim
ln -s $PWD/.bashrc ~/.bashrc
ln -s $PWD/.vim/vimrc ~/.vimrc
ln -s $PWD/git/.gitconfig ~/.gitconfig
ln -s $PWD/nvm/ ~/.nvm
# Compiling Command-T for Vim
echo Compiling Command-T for vim ...
cd  .vim/bundle/command-t/ruby/command-t/
ruby extconf.rb 
make
exit 0
